import React, { Component } from "react";
import { observer } from "mobx-react";
import glamorous, { Div } from "glamorous";

import searchStore from "../stores/SearchStore";

const SearchLink = glamorous.div({
  color: "darkmagenta",
  padding: "2px 8px",
});

@observer
class LastSearch extends Component {
  onItemClick = (item) => () => {
    const { onLastItemClick } = this.props;
    onLastItemClick(item);
  };

  render() {
    const list = searchStore.lastSearch.slice();
    return (
      <Div
        css={{
          margin: 40,
          marginBottom: 20,
          height: 90,
          width: 200,
          overflow: "auto",
        }}
      >
        <Div>Last queries:</Div>

        {list.length ? (
          list
            .filter((item) => !!item)
            .map((item) => {
              return (
                <SearchLink
                  onClick={this.onItemClick(item)}
                  css={{ textDecoration: "underline", cursor: "pointer" }}
                >
                  {item}
                </SearchLink>
              );
            })
        ) : (
          <SearchLink css={{ color: "black" }}>None</SearchLink>
        )}
      </Div>
    );
  }
}

export default LastSearch;
