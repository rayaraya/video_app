import { observable, action } from "mobx";

class SearchStore {
  @observable data = [];

  @action
  saveSearch(name) {
    this.data.push(name);
  }

  get lastSearch() {
    return this.data;
  }
}

const searchStore = new SearchStore();
export default searchStore;
