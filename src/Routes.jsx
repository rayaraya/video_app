import React, { Component } from "react";
import { Route } from "react-router-dom";

import { observer } from "mobx-react";
import Header from "./components/reusable/Header";
import VideoSearch from "./components/VideoSearch";
import About from "./components/About";
@observer
class Routes extends Component {
  render() {
    return (
      <div>
        <Header />
        <div>
          <Route exact path="/" component={VideoSearch} />
          <Route path="/about" component={About} />
        </div>
      </div>
    );
  }
}

export default Routes;
