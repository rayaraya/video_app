import React from "react";
import { Link } from "react-router-dom";
import { Div, H2 } from "glamorous";

const Header = () => (
  <Div
    css={{
      height: 100,
      background: "black",
      display: "flex",
      flexDirection: "column",
      padding: 40,
    }}
  >
    <H2 css={{ color: "white", fontFamily: "sans-serif" }}>Desktop Video</H2>
    <Div>
      <Link style={{ color: "yellow" }} to={"/"}>
        Home
      </Link>{" "}
      -|-
      <Link style={{ color: "yellow" }} to={"/about"}>
        About
      </Link>
    </Div>
  </Div>
);

export default Header;
