import React, { Component } from "react";
import { observer } from "mobx-react";
import glamorous, { Div } from "glamorous";

const VideoItem = glamorous.div({
  margin: 8,
});

@observer
class VideoGallery extends Component {
  getListItems = (videos) => {
    return videos.map((item) => {
      const splitedArray = item.split("=");
      const videoId = splitedArray[splitedArray.length - 1];
      const url = `https://www.youtube.com/embed/${videoId}`;
      return (
        <VideoItem>
          <iframe
            // width="560"
            // height="315"
            src={url}
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
          ></iframe>
        </VideoItem>
      );
    });
  };
  render() {
    const { videos } = this.props;
    return (
      <Div
        css={{
          margin: 40,
          display: "flex",
          flexWrap: "wrap",
        }}
      >
        {this.getListItems(videos)}
      </Div>
    );
  }
}

export default VideoGallery;
