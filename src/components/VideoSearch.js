import React, { Component } from "react";
import { observer } from "mobx-react";
import searchStore from "../stores/SearchStore";
import videosStore from "../stores/VideosStore";
import glamorous, { Div, Input, Button } from "glamorous";
import VideoGallery from "./VideoGallery";
import LastSearch from "./LastSearch";

import { fetchVideos } from "../api";
const SearchInput = glamorous(Input)({
  color: "pink",
});

const SearchButton = glamorous(Button)({
  color: "pink",
  marginLeft: 16,
  cursor: "pointer",
});

const SearchInputWrapper = glamorous.div({
  display: "flex",
  margin: 48,
  height: 24,
});

@observer
class VideoSearch extends Component {
  state = {
    inputVal: "",
    videos: [],
    loading: false,
  };

  onSearchClick = async () => {
    this.setState({ loading: true });
    const videos = await fetchVideos(this.state.inputVal);
    videosStore.setLastUrls(videos);
    this.setState({ loading: false });
    this.setState({ videos });
    searchStore.saveSearch(this.state.inputVal);
    this.setState({ inputVal: "" });
  };

  onLastItemClick = (item) => {
    this.setState({ inputVal: item });
    this.onSearchClick();
  };

  render() {
    const { videos, loading } = this.state;
    const lastVideos = videosStore.lastVideos;
    return (
      <Div>
        <Div css={{ display: "flex" }}>
          <SearchInputWrapper>
            <SearchInput
              placeholder="Type in video name.."
              type="text"
              value={this.state.inputVal}
              onChange={(e) => this.setState({ inputVal: e.target.value })}
            />
            <SearchButton onClick={this.onSearchClick} disabled={loading}>
              Search
            </SearchButton>
          </SearchInputWrapper>
          <LastSearch onLastItemClick={this.onLastItemClick} />
        </Div>
        {!loading ? (
          <VideoGallery videos={videos.length ? videos : lastVideos} />
        ) : (
          <Div
            css={{
              display: "flex",
              marginTop: 200,
              justifyContent: "center",
            }}
          >
            <svg
              version="1.1"
              id="loader-1"
              x="0px"
              y="0px"
              width="40px"
              height="40px"
              viewBox="0 0 50 50"
              style={{ enableBackground: "new 0 0 50 50" }}
            >
              <path
                fill="#000"
                d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z"
              >
                <animateTransform
                  attributeType="xml"
                  attributeName="transform"
                  type="rotate"
                  from="0 25 25"
                  to="360 25 25"
                  dur="0.6s"
                  repeatCount="indefinite"
                />
              </path>
            </svg>
          </Div>
        )}
      </Div>
    );
  }
}

export default VideoSearch;
