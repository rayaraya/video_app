import React, { Component } from "react";
import { observer } from "mobx-react";
import { Div } from "glamorous";

@observer
class About extends Component {
  render() {
    return (
      <Div
        css={{
          margin: 40,
          color: "lemonchiffon",
          backgroundColor: "indigo",
          height: 200,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          lineHeight: "26px",
          fontFamily: "monospace",
        }}
      >
        <Div>
          App is developed by Raisa Ryabinina, MIPT student, December 2020
          <br></br>
          Email: ryabinina.rb@mail.com
          <br></br>Tg: @rayaryabinina
          <br></br>Github: pixel-ray
        </Div>
      </Div>
    );
  }
}

export default About;
