import axios from "axios";
import { stringify } from "qs";
// import ElectronAdapter from "axios-electron-adapter";

// axios.defaults.adapter = ElectronAdapter;

axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded";

axios.defaults.headers.get["X-axios"] = "axios";
axios.defaults.headers.post["X-axios"] = "axios";
axios.defaults.headers.get["Content-Type"] =
  "application/x-www-form-urlencoded";
axios.defaults.headers.get["Access-Control-Allow-Origin"] = "*";

const checkResponseStructure = (response) => {
  if (response.data) {
    return response.data;
  } else {
    throw Error(`Failed response ${response.request.responseURL}`);
  }
};

const request = ({ method, path, data, query, params }) => {
  return axios({
    method,
    data,
    url: `${path}${query ? `?${stringify(query)}` : ""}`,
    ...params,
  })
    .then(checkResponseStructure)
    .catch((error) => {
      console.log(error);
      throw error;
    });
};

export const get = (path, query, params) =>
  request({
    path,
    method: "get",
    query,
    params,
  });

export const post = (path, data, query, params) =>
  request({
    path,
    data,
    method: "post",
    query,
    params,
  });
