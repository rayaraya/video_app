import { get } from "./axios";

export const fetchVideos = (topic) =>
  get(
    `http://bigone.demist.ru:1231/youtube/video/urlsbyprompt?prompt=${topic}&maxResult=10`
  );
