import { observable, computed, action } from "mobx";

class VideosStore {
  @observable lastUrls = [];

  @action
  setLastUrls(urls) {
    this.lastUrls = urls;
  }

  @computed
  get lastVideos() {
    return this.lastUrls;
  }
}

const videosStore = new VideosStore();
export default videosStore;
